﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(honirystore.web.Startup))]
namespace honirystore.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
